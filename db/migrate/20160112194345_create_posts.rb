class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :titre
      t.date :datepublication
      t.text :chapeau
      t.text :image
      t.belongs_to :auteur, index: true

      t.timestamps
    end
  end
end
