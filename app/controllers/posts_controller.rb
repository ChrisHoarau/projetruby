class PostsController < ApplicationController

	def show
		@post = Post.find(params[:id])
		@user = User.find(@post.user_id)
	end

	def create
		post = params[:post]
		post = post.permit(:titre, :datepublication, :chapeau, :contenu, :image, :user_id)

		@post = Post.new post

		if @post.save
			redirect_to [@post]
		else
			render 'new'
		end
	end

	def destroy
		post = Post.find(params[:id])

		post.delete

		redirect_to "/"
	end

	def edit
		@post = Post.find(params[:id])
	end

	def update 
		@post = Post.find(params[:id])

		post = params[:post]
		post = post.permit(:titre, :datepublication, :chapeau, :contenu, :image, :user_id)

		if @post.update_attributes(post)
			redirect_to [@post]
		else 
			render "edit"
		end
	end

end